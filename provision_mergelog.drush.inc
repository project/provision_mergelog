<?php

/**
 * @file
 *   Merge log from remotes server.
 *
 * This should do the following:
 *
 * 1. List all the Aegir clusters defined
 * 2. Copy the access.log files for the Aegir clusters locally
 * 3. Merge logs (using the mergelog binary) into the cluster.log files
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function provision_mergelog_drush_command() {
  $items = array();

  // The 'make-me-a-sandwich' command
  $items['provision-mergelog'] = array(
    'description' => "Merge logs from Aegir cluster servers.",
    'arguments' => array(
      'cluster' => 'The cluster to sync the files from (defaults to all)',
    ),
    'examples' => array(
      'drush provision-mergelog' => 'Merge logs for all clusters.',
      'drush @server_foo provision-mergelog' => 'Merge logs from the "foo" cluster.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  
  return $items;
}


/**
 * Main entry point
 *
 * XXX: this should iterate over the clusters, but now we assume it is passed as an argument
 */
function drush_provision_mergelog() {
  _drush_provision_mergelog_cluster(d()->name);
}

/*
 * 
 * This iterates over the provided cluster servers and copies files in place
 */
function _drush_provision_mergelog_cluster($cluster) {
  $c = d($cluster)->service('http');
  // look for a cluster service
  if (get_class($c) == 'provisionService_http_pack' || get_class($c) == 'provisionService_http_cluster') {
    $logsdir = drush_get_option('logsdir', d()->aegir_root . "/logs/");
    foreach (d()->master_web_servers as $server) {
      _drush_provision_mergelog_getlog($server);
      $logfiles[]= $logsdir . d($server)->remote_host . '.log';
    }
    foreach (d()->slave_web_servers as $server) {
      _drush_provision_mergelog_getlog($server);
      $logfiles[]= $logsdir . d($server)->remote_host . '.log';
    }
    drush_log("logfiles: " . join(",", $logfiles));
    drush_op_system('mergelog ' . join(" ", $logfiles) . " > " . $logsdir . d($cluster)->remote_host . '.log');
  }
}



function _drush_provision_mergelog_getlog($server) {
  $server = d($server);
  $logpath = drush_get_option('logpath', '/var/log/apache2/other_vhosts_access.log.1');
  $logsdir = drush_get_option('logsdir', $server->aegir_root . "/logs/");
  if (provision_is_local_host($server->remote_host)) {
    drush_log("copying local logfile in place for local server: ". $server->remote_host);
    provision_file()->copy($logpath, $logsdir . $server->remote_host . '.log');
  }
  else {
    drush_log("getting logs for server: " . $server->remote_host);
    # scp -B -q $script_user@$remote_host:$logpath $logsdir
    drush_shell_exec("scp -B -q %s@%s:%s %s/%s.log", $server->script_user, $server->remote_host, $logpath, $logsdir, $server->remote_host);
  }
}
